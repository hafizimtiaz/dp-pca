function sample = algo_PPM_laplace(A,R,Xinit,epsilon)
% Code by Hafiz Imtiaz, Anand Sarwate, Rutgers University, 2015.
% A - autocorrelation matrix of the data; A= X*X' columns of X are the samples 
% R - reduced dimension
% Xinit - d x R size, initialization for the sampling,
% should have orthonormal columns
% epsilon - privacy parameters
% d - data dimension

[~,S,~] = svd(A);

%% finding the iteration length
d=size(A,1);
sigma_orig=sort(diag(S),'descend');
param=sigma_orig(R)*log(d)/(sigma_orig(R)-sigma_orig(R+1));
L=round(10*param);

%% private power method
sigma=(10/epsilon)*R*L*sqrt(d);
lambda=sqrt(2)/sigma;
x_old=Xinit;
count=0;
siz=[d R];

while(count<=L)
    zz = rand(siz);
    xx = zeros(siz);
    in = zz <=.5;
    ip = zz > .5;
    xx(in) =  1/lambda *log(2*zz(in));
    xx(ip) = -1/lambda *log(2*(1-zz(ip)));
    
    G_new = xx;
    Y=A*x_old+G_new;
    count=count+1;
    [temp, ~]=qr(Y);
    x_old=[];
    x_old=temp(:,1:R);
end
sample=x_old;