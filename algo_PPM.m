function sample = algo_PPM(A,R,Xinit,epsilon,delta)
% Code by Hafiz Imtiaz, Anand Sarwate, Rutgers University, 2015.
% A - autocorrelation matrix of the data; A= X*X', columns of X are the samples 
% R - reduced dimension
% Xinit - d x R size, initialization for the sampling,
% should have orthonormal columns
% epsilon and delta - privacy parameters
% d - data dimension

[~,S,~] = svd(A);

%% finding the iteration length
d=size(A,1);
sigma_orig=sort(diag(S),'descend');
param=sigma_orig(R)*log(d)/(sigma_orig(R)-sigma_orig(R+1));
L=round(10*param);

%% private power method
sigma=(1/epsilon)*sqrt(4*R*L*log(1/delta));
x_old=Xinit;
count=0;
while(count<=L)
    G_new=normrnd(0,norm(x_old,Inf)*sigma,d,R);
    Y=A*x_old+G_new;
    count=count+1;
    [temp, ~]=qr(Y);
    x_old=[];
    x_old=temp(:,1:R);
end
sample=x_old;