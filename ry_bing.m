function y = ry_bing(y,l,n)
  
k=.5*(n-1.0);
  
for i=1:n
    omyi=1.0/(1.0-y(i)^2);
    smyi=sqrt(omyi);
    a=l(i)+l(i)*y(i)^2*omyi;
    for j=1:n
        a=a-l(j)*y(j)^2*omyi;
    end    
    theta=rtheta_bing(k,a) ;
    
    for j=1:n
        y(j)=y(j)*sqrt(1.0-theta)*smyi;
    end
    y(i)=sqrt(theta)*((-1.0)^round(rand(1)));
end