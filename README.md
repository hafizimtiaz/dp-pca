Differentially-private PCA Algorithms
=====================================
This package includes MATLAB implementations of some of the most recent differentially-private principal component analysis (PCA) algorithms. The list of the files in this repo is given below:

* algo_AG.m - implementaiton of the Analyze Gauss algorithm due to "Analyze Gauss: Optimal Bounds for Privacy-preserving Principal Component Analysis" by Dwork et al., 2014.
* algo_PPCA.m - implementaiton of the private PCA algorithm due to "A Near-optimal Algorithm for Differentially-private Principal Components" by Chaudhuri et al., 2013.
* algo_PPM.m and algo_PPM_laplace.m - implementaiton of the private power method algorithm due to "The Noisy Power Method: A Meta Algorithm with Applications" by Hardt and Price, 2014.
* my_vector_BMF_hoff.m - code for sampling a vector from the matrix-Bingham distribution. Required for PPCA algorithm. 
* RandOrthMat.m - code for generating a random orthogonal matrix of given size. [Ofek Shilon]
* rtheta_bing.m and ry_bing.m - helper codes for sampling from matrix-Bingham distribution. [Peter Hoff]


Feedback
--------

Please send bug reports, comments, or questions to [Hafiz Imtiaz](mailto:hafiz.imtiaz@outlook.com).
Contributions and extentions with new algorithms are welcome.
