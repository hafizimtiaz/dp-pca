function sample = algo_PPCA(A,R,Xinit)
% Code by Hafiz Imtiaz, Anand Sarwate, Rutgers University, 2015.
% A - sample covariance matrix of the data; A= (1/n) X*X'; X has the
% samples in columns
% n - number of samples
% R - reduced dimension
% Xinit - d x R size, initialization for the sampling,
% should have orthonormal columns
% d - data dimension

Xold=Xinit;

%% matrix-Bingham sampling starts from here
rnd_order=randsample(R,R);
for kk=1:length(rnd_order)
    k=rnd_order(kk);
    Xn=Xold;
    Xn(:,k)=[];
    N=null(Xn');
    z=N'*Xold(:,k);
    An=N'*A*N;
    zn = my_vector_BMF_hoff(An,z);
    Xold(:,k)=N*zn;
end
sample=Xold;