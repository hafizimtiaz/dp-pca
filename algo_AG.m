function [Apriv] = algo_AG(A,delta,epsilon)
% Code by Hafiz Imtiaz, Anand Sarwate, Rutgers University, 2015.
% A - covariance matrix of data = X*X'; X has the samples as columns
% delta & epsilon - privacy parameters
% Apriv - private version of A

D=(1/epsilon)*sqrt(2*log(1.25/delta));
d=size(A,1);
temp=normrnd(0,D,d,d);
temp2=triu(temp);
temp3=temp2';
temp4=tril(temp3,-1);
E=temp2+temp4;
Apriv=A+E;
