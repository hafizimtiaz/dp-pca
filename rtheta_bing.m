function th = rtheta_bing(k,a)
  
u=2;
g=k; 
lrth=0.0;
lrmx=0.0;
  
if a>0
    g= max([1.0/(1.0+log(2.0+a)),k-a]); 
    lrmx= a - k + g + (k-g)*log((k-g)/a);
end
  
while log(u)>lrth-lrmx
    u=rand(1);
    th=betarnd(.5,real(g));
    lrth=a*th+(k-g)*log(1-th);
end