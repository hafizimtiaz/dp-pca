function sample = my_vector_BMF_hoff(A,xinit)
% A - m x m autocorrelation matrix of the data; A= (1/n) X*X'
% m - length of each sample, n -number of samples
% maxiter - number of iteration for each sampling
% xinit - initialization for the sampling

xold=xinit(:);
m=size(A,1);

[V,S] = eig(A);
lambda=diag(S);

y=V'*xold;
sample=V*ry_bing(y,lambda,length(y));
sample=sample/norm(sample);

% rnd_order=randsample(m,m);
% for kk=1:length(rnd_order)
%     k=rnd_order(kk);
%     q=(y.^2)/(1-y(k)^2);
%     s=2*round(rand(1))-1;
%     th=my_grid_prob(lambda,k,q);
%     y=sign(y).*sqrt((1-th).*q);
%     y(k)=s*sqrt(th);
% end
% xnew=V*y;
% sample=xnew;